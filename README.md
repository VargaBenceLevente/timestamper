# TimeStamper

This is a little one-afternoon utility project of mine, aiming to assist in taking more useful audio (and video) recordings.

By setting the epoch to roughly match the time the recording started, the program allows for quickly adding timestamps to a text file, hopefully annotating the more interesting parts of the recording. Text can also be added to label or explain the entries.

The stamps have preceding hashmarks so that it can be quickly compiled as a Markdown file for additional usability.

## Compilation and running

Use Qt Creator or qmake to compile the project, then copy Qt5Core.dll, Qt5Gui.dll and Qt5Widgets.dll to the same folder that the executable is in.

If you are using Linux, I assume you don't need instructions to compile a simple Qt project.

## Usage

Try out each button and field, I believe that it should not need much of an explanation.

## Contributions

The project can be considered abandoned. There are too many problems with the given implementation and architecture to be further useful for anything more than a toy project.

If, by some miracle, you'd want to extend this program instead of making your own, better one, feel free to do so. Just make sure to give me credit for what I've done here.