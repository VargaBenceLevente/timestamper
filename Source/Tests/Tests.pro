QT += testlib
QT += gui widgets sql
CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

CONFIG += c++17

SOURCES +=  \
		../App/document.cpp \
                ../App/widgets.cpp \
		../App/documentmanager.cpp \
		../App/mainwindow.cpp \
		../App/settingsdialog.cpp \
    main_tests.cpp \
    test_basic.cpp \
    test_manager.cpp

HEADERS += ../App/document.h \
                ../App/widgets.h \
		../App/documentmanager.h \
		../App/mainwindow.h \
		../App/settingsdialog.h \
		../App/symbols.h \
    config.h \
    test_basic.h \
    test_manager.h

FORMS += \
		../App/mainwindow.ui \
		../App/settingsdialog.ui
