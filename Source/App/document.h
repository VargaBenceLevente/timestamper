#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <QString>
#include <QDateTime>
#include <QVector>

/// Represents an entire document
struct Document
{
    /// Represents a timestamp within a document
	struct Entry
	{
		QTime time;
		QString title;
		QString body;

        /// @returns Whether the contents of the entries match
        bool equals(const Entry& other) const { return time == other.time && title == other.title && body == other.body; }
	};
    /// Represents a point within a document
    struct SearchResult
	{
        /// Specifies the field for a search result
        enum class Type {time, title, body, subtitle};

        // -1 for the original body, starts from 0 for the list
        int entryIndex;
        Type whichField;
		int startsFrom;
	};
	/// Specifies where to search a document
	enum class searchResultType {title = 0b1, subtitle = 0b10, epoch = 0b100, text = 0b1000, entries = 0b10000};

    using KeyType = QString;

    // Fields
    KeyType key;
	QString title;
	QString subtitle;
	QDateTime epoch;
	QString text;
    QString numbering;
    int numberingStart;
	QVector<Document::Entry> entries;

public:
    /// Constructs a new document with the given information
    Document(QString title = "X", QString subtitle = "", QString text = "", QDateTime epoch = QDateTime::currentDateTime()) :
        title(title),
        subtitle(subtitle),
        epoch(epoch),
        text(text),
        numbering(""),
        numberingStart(1) {}

    /// @returns a string representation of the document in Markdown syntax
    QString toMarkdown(QString dateStyle = "yyyy.MM.dd hh:mm:ss") const;
    /// @returns a string representation of the document in plain text
    QString toPlainText(QString dateStyle = "yyyy.MM.dd hh:mm:ss") const;

    /// Searches the document for the given string
    /// @param searchFlags in which fields to search. Zero means every field
    /// @returns A list of indeces where a search result exists
    QVector<SearchResult> searchDocumentText(QString str, int searchFlags = 0) const;
    /// Searches the document for the given string
    /// @param searchFlags in which fields to search. Zero means every field
    /// @returns Whether there is at least one occurrence of the string in the given fields
    bool containsText(QString str, int searchFlags = 0) const;
    /// @returns Whether every field except for the key match
    bool equals(const Document& other) const;
    /// Copies all the fields of the other document into this one
    Document& operator=(const Document& other);
    /// copies all the CONTENT fields of the other document, EXCLUDING the key
    Document& makeEqualTo(const Document& other);
    /// @returns Whether this document is before the other document given an ordering
    /// @param orderBy The ordering
    bool isLessThan(const Document& other, const QStringList& orderBy) const;
};

/// @returns Whether the two documents represent the same file
inline bool operator==(const Document& lhs, const Document& rhs) { return lhs.key == rhs.key; }
/// @returns Whether the two documents represent the same file
inline bool operator!=(const Document& lhs, const Document& rhs) { return !(lhs==rhs); }

#endif // DOCUMENT_H
