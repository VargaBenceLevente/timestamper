#include "settingsdialog.h"
#include "mainwindow.h"
#include "widgets.h"
#include "ui_settingsdialog.h"

#include <QColorDialog>
#include <QSettings>
#include <QFileDialog>
#include <algorithm>
#include <QMap>

void SettingsDialog::saveSettings()
{
	QSettings setting("Cloud", "TimeStamper");
	// Document tab
	setting.setValue("DefaultDocumentTitle", ui->txtDocumentDefaultTitle->text());
    setting.setValue("DefaultDocumentSubtitle", ui->txtDocumentDefaultSubtitle->text());
	setting.setValue("DefaultDocumentDateStyle", ui->txtDocumentDefaultDateStyle->text());
	// Editor tab
    setting.setValue("WindowShowTopToolbar", ui->chkbxShowTopButtonBar->isChecked());
	setting.setValue("ShowButtonText", ui->chkbxShowButtonText->isChecked());
    setting.setValue("ShowDocumentBrowserInEditor", ui->chkbxShowDocumentBrowserInEditor->isChecked());
	setting.setValue("MainTextSize", ui->spinBoxTextSize->value());
	setting.setValue("MainTextFont", ui->fontComboBox->currentFont());
    setting.setValue("DocumentBrowserTextSize", ui->spinBoxDocumentBrowserTextSize->value());
    setting.setValue("DocumentBrowserMaxWidth", ui->spinBoxButtonMaxWidth->value());
    setting.setValue("NumberingWildcardChar", ui->txtNumberingWildcardLineEdit->text().length() > 0 ?
                         ui->txtNumberingWildcardLineEdit->text().left(1) : "%");
    setting.setValue("DocumentBrowserCollapsible", ui->chkbxDocumentBrowserCollapsible->isChecked());
	setting.setValue("EditorDisabled", ui->chkbxDisableEditor->isChecked());
    setting.setValue("DocumentBrowserScrollbar", ui->chkbxScrollbarDocumentBrowser->isChecked());
	// Storage tab
	setting.setValue("InnerSaveFileLocation", ui->txtInnerSaveFileLocation->text());
	// Search tab
    int searchFlags = 0;
    if(ui->chkbxSearchInTitle->isChecked())
        searchFlags += static_cast<int>(Document::searchResultType::title);
    if(ui->chkbxSearchInSubtitle->isChecked())
        searchFlags += static_cast<int>(Document::searchResultType::subtitle);
    if(ui->chkbxSearchInText->isChecked())
        searchFlags += static_cast<int>(Document::searchResultType::text);
    if(ui->chkbxSearchInEpoch->isChecked())
		searchFlags += static_cast<int>(Document::searchResultType::epoch);
	if(ui->chkbxSearchInEntries->isChecked())
		searchFlags += static_cast<int>(Document::searchResultType::entries);
    setting.setValue("SearchFlags", searchFlags);
    setting.setValue("SearchInstantly", ui->chkbxInstantSearch->isChecked());
	QStringList l;
	for(int i = 0; i < ui->listWidgetSortBy->count(); ++i)
		l.append(ui->listWidgetSortBy->item(i)->text());
	setting.setValue("SearchOrder", l);
	// General tab
	setting.setValue("WindowShowMaximized", ui->chkbxMaximizedOnStartup->isChecked());
	// make parent load settings
	emit settingSaved();
}

void SettingsDialog::goToPage(QString page)
{
	QMap<QString, int> indices = {
		{"document", 0},
		{"editor", 1},
		{"storage", 2},
		{"search", 3},
		{"general", 4}
	};
    if(indices.contains(page.toLower()))
        ui->tabWidget->setCurrentIndex(indices[page.toLower()]);
}

SettingsDialog::SettingsDialog(MainWindow& parent) :
	QDialog(&parent),
	ui(new Ui::SettingsDialog)
{
	ui->setupUi(this);

	// fill with default / current values
	QSettings setting("Cloud", "TimeStamper");

    // Document tab
	ui->txtDocumentDefaultTitle->setText(setting.value("DefaultDocumentTitle").toString());
    ui->txtDocumentDefaultSubtitle->setText(setting.value("DefaultDocumentSubtitle").toString());
	ui->txtDocumentDefaultDateStyle->setText(setting.value("DefaultDocumentDateStyle").toString());
	// Editor tab
    ui->chkbxShowTopButtonBar->setChecked(setting.value("WindowShowTopToolbar").toBool());
	ui->chkbxShowButtonText->setChecked(setting.value("ShowButtonText").toBool());
    ui->chkbxShowDocumentBrowserInEditor->setChecked(setting.value("ShowDocumentBrowserInEditor").toBool());
	ui->spinBoxTextSize->setValue(setting.value("MainTextSize").toInt());
	ui->fontComboBox->setCurrentFont(setting.value("MainTextFont").value<QFont>());
    ui->spinBoxDocumentBrowserTextSize->setValue(setting.value("DocumentBrowserTextSize").toInt());
    ui->spinBoxButtonMaxWidth->setValue(setting.value("DocumentBrowserMaxWidth").toInt());
    ui->txtNumberingWildcardLineEdit->setText(setting.value("NumberingWildcardChar").toString());
    ui->chkbxDocumentBrowserCollapsible->setChecked(setting.value("DocumentBrowserCollapsible").toBool());
	ui->chkbxDisableEditor->setChecked(setting.value("EditorDisabled").toBool());
    ui->chkbxScrollbarDocumentBrowser->setChecked(setting.value("DocumentBrowserScrollbar").toBool());
	// Storage tab
	ui->txtInnerSaveFileLocation->setText(setting.value("InnerSaveFileLocation").toString());
	// Search tab
	int searchFlags = setting.value("SearchFlags").toInt();
	ui->chkbxSearchInTitle->setChecked(searchFlags & static_cast<int>(Document::searchResultType::title));
	ui->chkbxSearchInSubtitle->setChecked(searchFlags & static_cast<int>(Document::searchResultType::subtitle));
	ui->chkbxSearchInText->setChecked(searchFlags & static_cast<int>(Document::searchResultType::text));
	ui->chkbxSearchInEpoch->setChecked(searchFlags & static_cast<int>(Document::searchResultType::epoch));
	ui->chkbxSearchInEntries->setChecked(searchFlags & static_cast<int>(Document::searchResultType::entries));
    ui->chkbxInstantSearch->setChecked(setting.value("SearchInstantly").toBool());
	ui->listWidgetSortBy->clear();
	ui->listWidgetSortBy->addItems(setting.value("SearchOrder").toStringList());
	// General tab
	ui->chkbxMaximizedOnStartup->setChecked(setting.value("WindowShowMaximized").toBool());

    // Set display
    if(setting.value("DocumentBrowserMaxWidth").toInt() == 0)
        ui->lblTextWidthInfo->setText("0 results in no limit.");
    else
        ui->lblTextWidthInfo->setText("");
}

SettingsDialog::~SettingsDialog()
{
	delete ui;
}

void SettingsDialog::on_buttonBox_accepted()
{
	saveSettings();
}

void SettingsDialog::on_btnApply_clicked()
{
	on_buttonBox_accepted();
}

void SettingsDialog::on_btnInnerSaveFileLocation_clicked()
{
	auto response = QFileDialog::getExistingDirectory(this, "Choose folder location", ui->txtInnerSaveFileLocation->text());
	if(response.size() > 0)
		ui->txtInnerSaveFileLocation->setText(response);
}

void SettingsDialog::on_listWidgetSortBy_itemDoubleClicked(QListWidgetItem *item)
{
	if(item->text().endsWith("(Ascending)"))
		item->setText(item->text().replace("Ascending", "Descending"));
	else
		item->setText(item->text().replace("Descending", "Ascending"));
}

void SettingsDialog::on_spinBoxButtonMaxWidth_valueChanged(int arg1)
{
    if(arg1 == 0)
        ui->lblTextWidthInfo->setText("0 results in no limit.");
    else
        ui->lblTextWidthInfo->setText("");
}

void SettingsDialog::on_txtNumberingWildcardLineEdit_textEdited(const QString &arg1)
{
    if(arg1.length() > 1)
        ui->txtNumberingWildcardLineEdit->setText(arg1.left(1));
}

void SettingsDialog::on_SettingsDialog_finished(int)
{
	emit closed();
}
