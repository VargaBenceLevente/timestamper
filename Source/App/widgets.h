#ifndef DOCUMENTENTRY_H
#define DOCUMENTENTRY_H

#include <QBoxLayout>
#include <QPushButton>
#include "documentmanager.h"
#include "document.h"

/// Represents a UI element displaying a Document::Entry
class DocumentEntry : public QVBoxLayout
{
    Q_OBJECT

	// setTabOrder necessitates access to pointers
public:
    class QLabel* entryNumber;
    class QTimeEdit* time;
    class QLineEdit* title;
    class QTextEdit* body;

	class QHBoxLayout* titleLayout;
	class QPushButton* btnToggleBodyVisibility;
	class QPushButton* btnDelete;

public:
    /// Constructs a new Vertical Layout, filled with the specified information
    explicit DocumentEntry(const Document::Entry& e = {QTime(0, 0), "", ""}, QString number = "", class MainWindow *parent = nullptr);
	~DocumentEntry();

    /// @returns The time displayed by the UI element
    QTime getTime() const;
    /// @returns The text displayed by the UI element
    QString getTitle() const;
    /// @returns The text displayed by the UI element
    QString getBody() const;

    /// Sets the font for the UI elements contained by the DocumentEntry
    void setFont(QFont font);
	/// Uses setTabOrder to chain this object's fields after the received widget
	void chainAfter(QWidget* widget);
	/// Uses setTabOrder to chain this object's fields after the received widget
	void chainAfter(DocumentEntry& entry);
	/// Appends text to the body widget, then shows it if there is anything to show
	void appendText(const QString& text);
    /// Moves focus to this element depending on the current state
	void setFocus();

signals:
    /// Signals to the parent that this entry should be deleted when appropriate
    void deleteThisEntry(DocumentEntry* e);

public slots:
    /// Hides or unhides the body UI element
    void toggleBodyVisibility();
    /// Sets the body height to accommodate for the whole document so a scrollbar is not displayed
	void recalculateBodyHeight();
    /// Emits the deleteThisEntry signal
    void deleteThis();
};

class QPushButtonFile : public QPushButton
{
    Q_OBJECT

    QString filename_;
    // Contents of the document the button represents
    QString title_;
    QString subtitle_;
    QDateTime epoch_;
public:
    QPushButtonFile(DocumentManager::KeyType key, QString text, MainWindow* parent);
    /// @returns The key of the document this button represents
    const QString& getKey() {return filename_;}
    /// Stores the parameters and recalculates text
    void setText(QString title, QString subtitle, QDateTime epoch, QString textPreview = "");
    /// Sets the button font as bold or non-bold and recalculates text
    void setBold(bool bold);
    /// Sets the font size of the button
    void setFontSize(int size);

private:
    /// Calculates the text that should be displayed and sets the button's current text
    void setDisplayText();

signals:
    void clicked(DocumentManager::KeyType key);

public slots:
    /// Sets the maximum width of the button, then recalculates text
	void recalculateWidth(int, int);

private slots:
    /// Emits the clicked signal with the appropriate parameter
    void on_clicked();
};

#endif // DOCUMENTENTRY_H
