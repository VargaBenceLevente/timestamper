#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    qRegisterMetaTypeStreamOperators<QList<int> >("QList<int>");

	MainWindow w;
	w.show();

	return a.exec();
}
