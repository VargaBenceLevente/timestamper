#include "widgets.h"

#include <QPushButton>
#include <QHBoxLayout>
#include <QTimeEdit>
#include <QLineEdit>
#include <QTextEdit>
#include <QSettings>
#include <QMessageBox>
#include <QSplitter>
#include <QLabel>
#include "mainwindow.h"

DocumentEntry::DocumentEntry(const Document::Entry &e, QString number, class MainWindow *parent) : QVBoxLayout(static_cast<QWidget*>(parent))
{
	titleLayout = new QHBoxLayout(parent);
	this->addLayout(titleLayout);

	time = new QTimeEdit(e.time, parent);
    time->setDisplayFormat("hh:mm:ss");
    time->setFixedHeight(30);
    time->setMinimumWidth(70);
	titleLayout->addWidget(time);

    entryNumber = new QLabel(number, parent);
    titleLayout->addWidget(entryNumber);

    title = new QLineEdit(parent);
    title->setFixedHeight(30);
    title->setPlaceholderText("Timestamp title");
    title->setText(e.title);
	titleLayout->addWidget(title);

    btnToggleBodyVisibility = new QPushButton(parent);
    btnToggleBodyVisibility->setIcon(QIcon(":/Images/Icons/up-arrow.png"));
	btnToggleBodyVisibility->setFixedSize(30, 30);
    btnToggleBodyVisibility->setToolTip("Hide timestamp description");
	connect(btnToggleBodyVisibility, SIGNAL(clicked()), this, SLOT(toggleBodyVisibility()));
	titleLayout->addWidget(btnToggleBodyVisibility);

    btnDelete = new QPushButton(parent);
	btnDelete->setFixedSize(30, 30);
    btnDelete->setIcon(QIcon(":/Images/Icons/cancel.png"));
    btnDelete->setToolTip("Delete this timestamp");
    connect(btnDelete, SIGNAL(clicked()), this, SLOT(deleteThis()));
    connect(this, &DocumentEntry::deleteThisEntry, parent, &MainWindow::deleteEntry);
	titleLayout->addWidget(btnDelete);

    body = new QTextEdit(parent);
    body->setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    body->setVisible(true);
    body->setText("A");
    recalculateBodyHeight();
	body->setText(e.body);
    if(e.body.length() == 0)
    {
        this->body->setVisible(false);
        btnToggleBodyVisibility->setIcon(QIcon(":/Images/Icons/down-arrow.png"));
        btnToggleBodyVisibility->setToolTip("Show timestamp description");
    }
    connect(body, SIGNAL(textChanged()), this, SLOT(recalculateBodyHeight()));
    this->addWidget(body);

    body->setTabChangesFocus(true);

    title->setFocus();
}

DocumentEntry::~DocumentEntry()
{
    if(entryNumber)
        delete entryNumber;
	delete time;
	delete title;
	delete body;
	delete titleLayout;
	delete btnToggleBodyVisibility;
	delete btnDelete;
}

QTime DocumentEntry::getTime() const
{
	return time->time();
}

QString DocumentEntry::getTitle() const
{
	return title->text();
}

QString DocumentEntry::getBody() const
{
	return body->toPlainText();
}

void DocumentEntry::setFont(QFont font)
{
    QFont fontNumber = font;
    fontNumber.setPointSize(font.pointSize() - 2);
    entryNumber->setFont(fontNumber);
    body->setFont(font);
    title->setFont(font);
    time->setFont(font);

	// With the newly acquired font, recalculate height necessary to show fields
	int titleHeight = static_cast<int>(QFontMetrics(font).height());
	time->setFixedHeight(titleHeight + 3);
	title->setFixedHeight(titleHeight + 3);
	btnDelete->setFixedSize(titleHeight + 3, titleHeight + 3);
	btnToggleBodyVisibility->setFixedSize(titleHeight + 3, titleHeight + 3);
	recalculateBodyHeight();
}

void DocumentEntry::chainAfter(QWidget *widget)
{
    widget->setTabOrder(widget, title);
	widget->setTabOrder(title, body);
}

void DocumentEntry::chainAfter(DocumentEntry &entry)
{
	chainAfter(entry.body);
}

void DocumentEntry::appendText(const QString &text)
{
	body->append(text);
	body->setVisible(body->toPlainText().length() > 0);
}

void DocumentEntry::setFocus()
{
	if(body->isVisible())
		body->setFocus();
	else
		title->setFocus();
}

void DocumentEntry::toggleBodyVisibility()
{
    this->body->setVisible(!body->isVisible());
    if(body->isVisible())
    {
        btnToggleBodyVisibility->setIcon(QIcon(":/Images/Icons/up-arrow.png"));
        btnToggleBodyVisibility->setToolTip("Hide timestamp description");
    }
    else
    {
        btnToggleBodyVisibility->setToolTip("Show timestamp description");
        btnToggleBodyVisibility->setIcon(QIcon(":/Images/Icons/down-arrow.png"));
    }

}

void DocumentEntry::recalculateBodyHeight()
{
    int height = std::max(static_cast<int>(QTextDocument("A", this).size().height()), static_cast<int>(body->document()->size().height()));
    body->setFixedHeight(height + 3);
}

void DocumentEntry::deleteThis()
{
    if(QMessageBox::question(parentWidget(), "Delete?", "Are you sure you want to delete this entry?") == QMessageBox::Yes)
        emit deleteThisEntry(this);
}

QPushButtonFile::QPushButtonFile(DocumentManager::KeyType key, QString text, MainWindow *parent) :
    QPushButton(text, parent), filename_(key)
{
    connect(this, SIGNAL(clicked()), this, SLOT(on_clicked()));
}

void QPushButtonFile::setText(QString title, QString subtitle, QDateTime epoch, QString textPreview)
{
    title_ = title;
    subtitle_ = subtitle;
    epoch_ = epoch;
    setDisplayText();

    if(textPreview.length() > 0)
        setToolTip(textPreview);
}

void QPushButtonFile::setBold(bool bold)
{
    QFont f = this->font();
    f.setBold(bold);
    this->setFont(f);
    if(bold)
        setDisplayText();
}

void QPushButtonFile::setFontSize(int size)
{
    QFont f = this->font();
    f.setPointSize(size);
    this->setFont(f);
}

void QPushButtonFile::setDisplayText()
{
    QSettings setting("Cloud", "TimeStamper");
    int maxLength = font().bold() ? (width() / 8 + 5) : width() / 7 + 5;
    if(setting.value("DocumentBrowserMaxWidth").toInt() > 0 && maxLength > setting.value("DocumentBrowserMaxWidth").toInt())
        maxLength = setting.value("DocumentBrowserMaxWidth").toInt();
    QString buttonText = "";

    // title
    QString titleText = title_;
    if(titleText.length() - maxLength > 0)
    {
        titleText.chop(titleText.length() - maxLength);
        titleText.append("...");
    }
    buttonText.append(titleText);

    // subtitle
    if(subtitle_.length() > 0)
    {
        QString subtitleText = subtitle_;
        if(subtitleText.length() > maxLength)
        {
            subtitleText.chop(subtitleText.length() - maxLength);
            subtitleText.append("...");
        }
        buttonText.append("\n").append(subtitleText);
    }

    // epoch
    buttonText.append("\n" + epoch_.toString("yyyy MMM dd hh:mm"));

    QAbstractButton::setText(buttonText);
}

void QPushButtonFile::recalculateWidth(int pos, int)
{
	setMaximumWidth(static_cast<MainWindow*>(parent())->window()->width() - pos - 110);
    setDisplayText();
}

void QPushButtonFile::on_clicked()
{
    emit clicked(filename_);
}
